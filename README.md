# Darmanin

## Qu'est ce que ça fait ?

Floute 'violences policières', 'la police tue' and 'la police ment' et rajoute un petit emoji ensuite ( 🤕, 💀 ou 🤥 ) pour quand même avoir une idée de quoi parlait le truc flouté.

## Comment l'installer ?

Il est déjà au gouvernement.

Plus sérieusement, c'est disponible sur https://addons.mozilla.org/fr/firefox/addon/darmanin-le-flou-des-violences

## Comment contribuer ?

Aucune idée. Si vous avez des propositions, je peux y réfléchir, pour l'instant je suis seul à bord mais si d'autres personnes veulent s'impliquer sur cette blague, on peut changer la gouvernance.

Si vous trouvez un bug, vous pouvez me contacter par ici, je n'ai aucune idée de quand je vais tomber sur le message. Ou encore sur le fédiverse : @ronane@mamot.fr
