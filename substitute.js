/* This extension replace Macron with ₥₳₡₹¤₦ */

let regexViolences = new RegExp("[#]?violence[s]?[ ]?polici[èe]re[s]?", 'gui');
let regexTue = new RegExp("[#]?la[ ]?police[ ]?tue", 'gui');
let regexMent = new RegExp("[#]?la[ ]?police[ ]?ment", 'gui');

let allRegexs = [regexViolences, regexTue, regexMent];

/**
 * Substitutes macron into text nodes.
 * If the node contains more than just text (ex: it has child nodes),
 * call replaceText() on each of its children.
 *
 * @param  {Node} node    - The target DOM Node.
 * @return {void}         - Note: the emoji substitution is done inline.
 */
function replaceText (node) {
  // Setting textContent on a node removes all of its children and replaces
  // them with a single text node. Since we don't want to alter the DOM aside
  // from substituting text, we only substitute on single text nodes.
  // @see https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent
  if (node.nodeType === Node.TEXT_NODE) {
    // This node only contains text.
    // @see https://developer.mozilla.org/en-US/docs/Web/API/Node/nodeType.

    // Skip textarea or input nodes due to the potential for accidental submission
    // of substituted macron where none was intended.
    if (node.parentNode &&
        ((node.parentNode.nodeName === 'TEXTAREA') ||
        (node.parentNode.nodeName === 'INPUT'))) {
      return;
    }

    // Because DOM manipulation is slow, we don't want to keep setting
    // textContent after every replacement. Instead, manipulate a copy of
    // this string outside of the DOM and then perform the manipulation
    // once, at the end.
    if (!node.parentNode.classList.contains('violpol')) {
    	let content = node.textContent;
	let found = false;
	
	for (const regex of allRegexs) {
		found = content.match(regex);
		if (found) {
			break;
		}
	}

	if (found) {
		console.log("trouvé dans "+ content);
		var parent = node.parentNode;
		parent.textContent = '';
		
		let splitContent = [content];
		for (const regex of allRegexs) {
			console.log(regex);
			console.table(splitContent);
			for (let i = 0; i < splitContent.length; i++) {
				let match = splitContent[i].match(regex);
				if (match) {
					let splited =  splitContent[i].split(regex);
					let totalParts = splited.length + match.length;
					let newSplited = [];
					let splitIndex = 0;
					let matchIndex = 0;
					for (let y = 0; y < totalParts; y++) {
						let element = null;
						if (splitIndex == matchIndex) {
							element = splited[splitIndex];
							splitIndex++;
						} else {
							element = match[matchIndex];
							matchIndex++;
						}
						newSplited.push(element);
					}
					splitContent[i] = newSplited;
				}
			}
			splitContent = splitContent.flat();
		}

		
		splitContent.forEach(function(item, index, array) {
			
			let textNode = null;
			let emoji = null;
			if (item.match(regexViolences)) {
				textNode = document.createElement('span');
				textNode.classList.add('violpol');
				textNode.textContent = item; 
				emoji = String.fromCodePoint(0x1F915);
			} else if (item.match(regexTue)) {
				textNode = document.createElement('span');
				textNode.classList.add('violpol');
				textNode.classList.add('tue');
				textNode.textContent = item; 
				emoji = String.fromCodePoint(0x1F480);
			} else if (item.match(regexMent)) {
				textNode = document.createElement('span');
				textNode.classList.add('violpol');
				textNode.textContent = item; 
				emoji = String.fromCodePoint(0x1F925);
			} else {
				textNode = document.createTextNode(item);
			}
			console.log(textNode);
			parent.appendChild(textNode);
			if (emoji) {
				parent.appendChild(document.createTextNode(emoji));
			}
		});
	}

    }
  }
  else {
    // This node contains more than just text, call replaceText() on each
    // of its children.
    for (let i = 0; i < node.childNodes.length; i++) {
      replaceText(node.childNodes[i]);
    }    
  }
}

// Start the recursion from the body tag.
replaceText(document.body);

function checkNodeIsSomeFacebookShit(node) {
  if (node && node.nodeName === "BODY")
	return false;
  else if (node && node.attributes && node.attributes.getNamedItem('contenteditable'))
	return true;
  else 
    return checkNodeIsSomeFacebookShit(node.parentNode);
}


// Now monitor the DOM for additions and substitute macron into new nodes.
// @see https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver.
const observer = new MutationObserver((mutations) => {
  mutations.forEach((mutation) => {

    if (mutation.addedNodes && mutation.addedNodes.length > 0) {

      // This DOM change was new nodes being added. Run our substitution
      // algorithm on each newly added node.
      for (let i = 0; i < mutation.addedNodes.length; i++) {
        const newNode = mutation.addedNodes[i];
		if (!checkNodeIsSomeFacebookShit(newNode))
		  replaceText(newNode);
		else 
		  console.log("facebook shit detected");
      }
    }
  });
});


observer.observe(document.body, {

  childList: true,
  subtree: true
});
